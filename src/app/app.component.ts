import { Component } from '@angular/core';
import { AuthService } from './_services/auth.service';
import { Router } from '@angular/router';
import { UserService} from './_services/user.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor(private userService: UserService, private authService: AuthService , router: Router){
    authService.user$.subscribe(user => {
  if(user)
      {
        userService.save(user);

      let returnURL = localStorage.getItem('returnURL');

      if(returnURL){
        localStorage.removeItem('returnURL');
        router.navigateByUrl(returnURL);
      }
       
      }
    });
  }
}

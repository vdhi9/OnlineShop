import { Injectable } from '@angular/core';

import { AngularFireDatabase } from 'angularfire2/database'; 
import { Observable } from 'rxjs/Observable';
import { Product } from '../_model/product';
@Injectable()
export class ProductService {

  constructor(private db: AngularFireDatabase) { 
   
  }

  create(product)
  {
   return this.db.list('/items').push(product);
  }

  getAll()
  {
      return this.db.list('/items');//.valueChanges();
      //The .valueChanges() method returns a synchronized array of JSON objects. 
  }

  get(productId){
   // debugger;
    return this.db.object('/items/' + productId);//.valueChanges();

  }

  update(productId,product){

    return this.db.object('/items/'+ productId).update(product);

  }

  delete(productId){


    return this.db.object('/items/' + productId).remove();
  }


}

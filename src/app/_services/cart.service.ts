import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database'; 
import { Observable } from 'rxjs/Observable';
import { Product } from '../_model/product';
import { AuthService} from '../_services/auth.service';
import { User } from '../_model/user';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import * as firebase from 'firebase';
import { Cart } from '../_model/cart';
import { ShoppingCart } from '../_model/shopping-cart';
import 'rxjs/Observable';

@Injectable()
export class CartService{
  user: string;
  carts:Cart[];
  fc: Cart[];
  userId: string;
  product: Product[] = [];
   count: number =0;
  constructor(private db: AngularFireDatabase, private authService: AuthService) { 
   
  }

  private getItem(cartId: string, productId: string){
    debugger;
    return this.db.object('/cart/' + cartId + '/items/' + productId);
  }

  async addToCart(product: Product){
    var user = firebase.auth().currentUser;
    if(user)  {
     
      this.user = user.email;
      this.authService.user$.subscribe(user => this.userId = user.uid);
      let iresult = await this.db.list('/cart/'+this.userId);
      
     
      if(!iresult){
      this.db.list('/cart/'+this.userId).push({
        "user":this.user
      });
    }
    }
    this.updateItemQuantity(product,1);
    
  }

  async removeFromCart(product:Product){

    this.updateItemQuantity(product,-1);
   }

  private async updateItemQuantity(product: Product, change: number){
    let item$ = this.getItem(this.userId, product.$key)

    item$.take(1).subscribe(item => {
       item$.update({ product: product, quantity: (item.quantity || 0)  + change});
      
    });
  }

   async getCart():  Promise<Observable<ShoppingCart>>
    {
     // var user = firebase.auth().currentUser;
     // if(user)  {
      //this.authService.user$.subscribe(user => this.userId = user.uid);
      return this.db.object("/cart/"+this.userId).map( x => new ShoppingCart(x.items));
     // }
  }
}

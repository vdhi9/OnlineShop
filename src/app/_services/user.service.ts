import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import {  FirebaseObjectObservable } from 'angularfire2/database';//-deprecated';
import * as firebase from 'firebase';
import { User} from '../_model/user';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {
 
  constructor(private db: AngularFireDatabase) { 
    
  }

  save (user: firebase.User){
    this.db.object('/users/' + user.uid).update({
      name: user.displayName,
      email: user.email,
      
    });
  }

   get(uid: string)//:Observable<User>//: FirebaseObjectObservable<User>
   {
      return this.db.object('/users/'+uid);//.valueChanges();

    }
}





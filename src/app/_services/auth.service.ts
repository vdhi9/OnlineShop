import { Injectable } from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { User } from '../_model/user';
import { UserService } from './user.service';

//declare var firebase:any;
var provider = new firebase.auth.GoogleAuthProvider();
@Injectable()
export class AuthService {
  public userName: string;
  public userEmail:string;
 public isAdmin:boolean;
 user: any;
   user$ : Observable<firebase.User>;

    constructor(private afAuth: AngularFireAuth, private route: ActivatedRoute, private userService: UserService) {    
      this.user$ =  afAuth.authState;   
     }

  login(){
    let returnURL = this.route.snapshot.queryParamMap.get('returnURL') || '/';
     localStorage.setItem('returnURL',returnURL);
  this.afAuth.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
    // firebase.auth().signInWithPopup(provider).then(function(result) {
    //   // This gives you a Google Access Token. You can use it to access the Google API.
    //   var token = result.credential.accessToken;
    //   // The signed-in user info.
    //   var user = result.user;
    //   // ...
    // }).catch(function(error) {
    //   // Handle Errors here.
    //   var errorCode = error.code;
    //   var errorMessage = error.message;
    //   // The email of the user's account used.
    //   var email = error.email;
    //   // The firebase.auth.AuthCredential type that was used.
    //   var credential = error.credential;
    //   // ...
    // });
  }

  logout(){
    firebase.auth().signOut().then(function() {
      // Sign-out successful.
    }).catch(function(error) {
      // An error happened.
    });
  }

  isAuthenticated(){
    var user = firebase.auth().currentUser;
   
        if (user) {  
            this.userService.get(user.uid).subscribe(user => this.user= user);       
            this.userName=user.displayName;
            this.userEmail = user.email;
            this.isAdmin = this.user.isAdmin;
            
         return true;
    } else {
    return false;
    }
}
}
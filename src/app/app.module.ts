import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '../environments/environment';
import { RouterModule, Routes } from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AppRoutesModule } from './app.routes';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';

import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {CategoriesService} from './_services/categories.service';
import {AuthService} from './_services/auth.service';
import { ProductComponent } from './product/product.component';
import { AdmProductComponent } from './admin/adm-product/adm-product.component';
import { ProductDetailsComponent } from './admin/product-details/product-details.component';
import { ProductService} from './_services/product.service';
import { UserService} from './_services/user.service';
import { FormsModule } from '@angular/forms';
import { CartComponent } from './cart/cart.component';
import { CartService } from './_services/cart.service';
import { AdminGuardService } from './_guards/adm.guard';
import { MainComponent } from './main/main.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
   
    ProductComponent,
    AdmProductComponent,
    ProductDetailsComponent,
    CartComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    AppRoutesModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    NgbModule.forRoot()
  ],
  providers: [AuthService, CategoriesService, UserService, ProductService, CartService, AdminGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { CartService } from '../_services/cart.service';
import { User} from '../_model/user';
import { Observable } from 'rxjs/Observable';
import { ShoppingCart } from '../_model/shopping-cart';
//import { callUserCallback } from '@firebase/database/dist/esm/src/core/util/util';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userName:string;
  userEmail:string;
  isAdmin:boolean;
  cart$:Observable<ShoppingCart>;
    constructor(private authService: AuthService, private cartService: CartService) { 
   
  }
  user:any;
  length:number;
 async ngOnInit() {
    this.authService.user$.subscribe(user => this.user = user);

    this.cart$ = await this.cartService.getCart();
    
    
  }

  onLogin(){
    this.authService.login();
  }

  onLogout(){
    this.authService.logout();
  }


  isAuthenticated(){
    let result = this.authService.isAuthenticated();
    if(result){
      this.userEmail = this.authService.userEmail;
      this.userName = this.authService.userName;
      console.log("admin detail "+this.authService.isAdmin);
      this.isAdmin = this.authService.isAdmin;
    }
   return result;
  }

}

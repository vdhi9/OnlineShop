import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { ProductService } from '../_services/product.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../_model/product';
import { CategoriesService } from '../_services/categories.service';
import { CartService } from '../_services/cart.service';
import { AuthService } from '../_services/auth.service';
import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy {
  categories$;
  products: any[] = [];
  fp:any[] = [];
  category: string;
  cart: any;
  subscription: Subscription;
  constructor(private productService: ProductService, 
    private route: ActivatedRoute, 
    private categoryservice: CategoriesService, 
    private cartService: CartService, 
    private authService: AuthService,
    private router: Router) {
   
    this.categories$ = this.categoryservice.getCategories();
    this.productService.getAll().subscribe(product =>
      {
        
         this.products = product;
         
          route.queryParamMap.subscribe(params => {
          this.category = params.get('category');
             
           this.fp = (this.category) ? 
           this.products.filter(p => p.category === this.category) : 
           this.products;
    
         });

      });
   }

 async ngOnInit() {
    this.subscription= (await this.cartService.getCart()).subscribe(cart => this.cart = cart);
  }

  ngOnDestroy(){

    this.subscription.unsubscribe();
}

}

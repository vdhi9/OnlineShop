import { Component, OnInit } from '@angular/core';
import { ProductService} from '../../_services/product.service';
import { Product } from '../../_model/product';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
//import { AngularFireList } from 'angularfire2/database';
import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'app-adm-product',
  templateUrl: './adm-product.component.html',
  styleUrls: ['./adm-product.component.css']
})
export class AdmProductComponent implements OnInit {
  products:Product[];
  subscription: Subscription;
  fp:any[];
  constructor(private productService: ProductService) { 
    this.subscription = this.productService.getAll().subscribe(products => this.fp  = products);
  }

  filter(query: string){
    
    this.fp = (query) ?
    this.fp.filter(p => p.title.toLowerCase().includes(query.toLowerCase())) : 
    this.fp;
   }

  ngOnInit() {
    //this.productService.getAll().subscribe(products => this.fp = products);
   
      
     
  }

}

import { Component, OnInit } from '@angular/core';
import {ProductService} from '../../_services/product.service';
import { Router,ActivatedRoute } from '@angular/router';
import { CategoriesService} from '../../_services/categories.service';
import 'rxjs/add/operator/take';
@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  categories$;
  product={};
  id;
  constructor(private productService: ProductService,
    private router: Router, 
    private route: ActivatedRoute,
    private categoryService: CategoriesService ) { }

  ngOnInit() {
    this.categories$ = this.categoryService.getCategories();
    this.id= this.route.snapshot.paramMap.get('id');
    if(this.id) this.productService.get(this.id).take(1).subscribe(p=> this.product=p);
  }

  save(product){

    if (this.id){
      this.productService.update(this.id,product);
    }
    else{
      this.productService.create(product);
    }

this.router.navigate(['/admin/products']);
  }
}

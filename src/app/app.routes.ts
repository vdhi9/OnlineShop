import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AdmProductComponent} from './admin/adm-product/adm-product.component';
import {ProductDetailsComponent} from './admin/product-details/product-details.component';
import { ProductComponent} from './product/product.component';
import { CartComponent} from './cart/cart.component';
import { AdminGuardService } from './_guards/adm.guard';
import { MainComponent} from './main/main.component';
const appRoutes: Routes = [
    {path:'', component: MainComponent},
    {path:'shop', component: MainComponent},
    {path: 'admin/products', component: AdmProductComponent, canActivate: [AdminGuardService]},
    {path: 'admin/product/new', component: ProductDetailsComponent, canActivate: [AdminGuardService]},
    {path: 'admin/product/:id', component: ProductDetailsComponent, canActivate: [AdminGuardService]},
    {path: 'cart', component: CartComponent}
  ];
  
  @NgModule({
    imports: [
      RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]
  })
  export class AppRoutesModule { }
  
import { Product } from "./product";
export interface Cart {

   $key:string;
   products: Product[];
   user: string

}
import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { ProductService } from '../_services/product.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../_model/product';
import { CategoriesService } from '../_services/categories.service';
import { CartService } from '../_services/cart.service';
import { AuthService } from '../_services/auth.service';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

   category: string;
  fp:any[] = [];
  products: any[] = [];
  categories$;
  @Input('shopping-cart') shoppingCart;
  @Input('product') product: Product;
  constructor(private productService: ProductService, 
    private route: ActivatedRoute, 
    private categoryservice: CategoriesService, 
    private cartService: CartService, 
    private authService: AuthService,
    private router: Router) {
  
    this.categories$ = this.categoryservice.getCategories();
    this.productService.getAll().subscribe(product =>
      {
       
         this.products = product;
         
          route.queryParamMap.subscribe(params => {
          this.category = params.get('category');
             
           this.fp = (this.category) ? 
           this.products.filter(p => p.category === this.category) : 
           this.products;
    
         });

      });
   }

  ngOnInit() {
  }

  addToCart(product: Product){
    console.log("result: "+this.authService.isAuthenticated());
    if(this.authService.isAuthenticated()){
     this.cartService.addToCart(product);
    }
    else{
      this.authService.login();
    }
  }

  getQuantity(){
   
    if(!this.shoppingCart) return 0;

    let item= this.shoppingCart.items[this.product.$key];
    return item ? item.quantity : 0; 
  }

  removeFromCart(){
    this.cartService.removeFromCart(this.product);
    
  }

}

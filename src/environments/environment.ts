// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBNbYNO-_clPnqzyux3_TJWYPkFXdl6tpY",
    authDomain: "auth-e620c.firebaseapp.com",
    databaseURL: "https://auth-e620c.firebaseio.com",
    projectId: "auth-e620c",
    storageBucket: "auth-e620c.appspot.com",
    messagingSenderId: "1060335375738"
  }

};
